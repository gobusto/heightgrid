/**
@file
@brief Functions and structures used to generate a heightgrid.

Copyright (C) 2018 Thomas Glyn Dennis.

Permission is hereby granted, free of charge, to any person
obtaining a copy of this software and associated documentation
files (the "Software"), to deal in the Software without
restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following
conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.
*/

#ifndef __HEIGHTGRID_H__
#define __HEIGHTGRID_H__

#ifdef __cplusplus
extern "C" {
#endif

#define HG_TRUE 1   /**< @brief Boolean "true" value. */
#define HG_FALSE 0  /**< @brief Boolean "false" value. */

typedef double hg_real;         /**< @brief Determines float-value accuracy. */
typedef unsigned long hg_int;   /**< @brief Integer type used for grid size. */
typedef unsigned char hg_bool;  /**< @brief Used to indicate success/errors. */

/**
@brief Represents a grid of height values.
*/

typedef struct {
  hg_int x_size;  /**< @brief The number of points along the "X" axis. */
  hg_int y_size;  /**< @brief The number of points along the "Y" axis. */
  hg_real *point; /**< @brief The actual height values, as a 1D array. */
} HeightGrid;

/**
@brief Create a new HeightGrid structure.

@param x The number of height-points along the X axis.
@param y The number of height-points along the Y axis.
@return A new HeightGrid structure on success, or NULL on failure.
*/

HeightGrid *hgCreate(hg_int x, hg_int y);

/**
@brief Destroy a previously-created HeightGrid structure.

@param hg The HeightGrid structure to be destroyed.
@return Always returns NULL.
*/

HeightGrid *hgDelete(HeightGrid *hg);

/**
@brief Set the height value for a specific X/Y point.

@param hg The HeightGrid structure to modify.
@param x The X coordinate to be modified.
@param y The Y coordinate to be modified.
@param value The new height value for the specified point.
@return Returns HG_TRUE on success, or HG_FALSE if something went wrong.
*/

hg_bool hgSetValue(HeightGrid *hg, hg_int x, hg_int y, hg_real value);

/**
@brief Get the height value for a specific X/Y point.

@param hg The HeightGrid structure to check.
@param x The X coordinate to be checked.
@param y The Y coordinate to be checked.
@return The height value at the coordinate specified.
*/

hg_real hgGetValue(const HeightGrid *hg, hg_real x, hg_real y);

/**
@brief Export a HeightGrid structure as an OBJ mesh file.

@param hg The HeightGrid structure to be exported.
@param file_name The name of the file to save the exorted data as.
@return Returns HG_TRUE on success, or HG_FALSE if something went wrong.
*/

hg_bool hgExportOBJ(const HeightGrid *hg, const char *file_name);

#ifdef __cplusplus
}
#endif

#endif /* __HEIGHTGRID_H__ */
