HeightGrid
==========

A simple way to represent a grid-of-points, with the ability to export to OBJ.

Useful for simple videogame landscapes.

Getting height values
---------------------

It's possible to get the height values _between_ points:

    hg_real result = hgGetValue(hg, 1.2, 4.7);

This is calculated on the following basis:

    ^
    |
    Y
     __ __
    |\ |\ |
    |_\|_\|
    |\ |\ |
    |_\|_\| X->

In other words, if a positive X axis points to the right, and a positive Y axis
points upwards (with the Z axis corresponding to the actual "height" values), a
quartet of neighbouring points is represented as a pair of triangles, the first
using the lower-left vertex and the second using the upper-right vertex.

Note that this is also how the grid is represented if it is exported as an OBJ.

Licensing
---------

This code is made available under the [MIT](http://opensource.org/licenses/MIT)
licence, allowing you to use or modify it for any purpose, including commercial
and closed-source projects. All I ask in return is that _proper attribution_ is
given (i.e. don't remove my name from the copyright text in the source code and
perhaps include me on the "credits" screen, if your program has such a thing).
