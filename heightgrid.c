/**
@file
@brief Functions and structures used to generate a heightgrid.

Copyright (C) 2018 Thomas Glyn Dennis.

Permission is hereby granted, free of charge, to any person
obtaining a copy of this software and associated documentation
files (the "Software"), to deal in the Software without
restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following
conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "heightgrid.h"

/*
[PUBLIC] Create a new HeightGrid structure.
*/

HeightGrid *hgCreate(hg_int x, hg_int y) {
  HeightGrid *hg;

  if (x < 1 || y < 1)
    return NULL;

  hg = (HeightGrid*)malloc(sizeof(HeightGrid));
  if (!hg)
    return NULL;

  hg->point = (hg_real*)malloc(sizeof(hg_real) * x * y);
  if (!hg->point)
    return hgDelete(hg);

  hg->x_size = x;
  hg->y_size = y;
  return hg;
}

/*
[PUBLIC] Destroy a previously-created HeightGrid structure.
*/

HeightGrid *hgDelete(HeightGrid *hg) {
  if (!hg)
    return NULL;

  free(hg->point);
  free(hg);
  return NULL;
}

/*
[PUBLIC] Set the height value for a specific X/Y point.
*/

hg_bool hgSetValue(HeightGrid *hg, hg_int x, hg_int y, hg_real value) {
  if (!hg || x >= hg->x_size || y >= hg->y_size)
    return HG_FALSE;

  hg->point[(y * hg->x_size) + x] = value;
  return HG_TRUE;
}

/**
@brief Convert a coordinate into a pair of this/next tile indices + lerp value.

@param num_points The number of points across this axis (X or Y).
@param value The original how-far-across-the-axis-are-we value.
@param a Stores the first index value.
@param b Stores the second index value.
@return The how-far-between those two indices-are-we value.
*/

static hg_real hgClamp(hg_int num_points, hg_real value, hg_int *a, hg_int *b) {
  if (num_points < 1 || !a || !b)
    return 0;

  if (value <= 0) {
    *a = 0;
    *b = 0;
    return 0;
  }

  if (value >= num_points - 1) {
    *a = num_points - 1;
    *b = num_points - 1;
    return 0;
  }

  *a = (hg_int)value;
  *b = *a + 1;
  return value - *a;
}

/*
[PUBLIC] Get the height value for a specific X/Y point.
*/

hg_real hgGetValue(const HeightGrid *hg, hg_real x, hg_real y) {
  hg_int x1, x2, y1, y2;
  hg_real a, b, c, d;

  if (!hg)
    return 0;

  x = hgClamp(hg->x_size, x, &x1, &x2);
  y = hgClamp(hg->y_size, y, &y1, &y2);

  a = hg->point[(y1 * hg->x_size) + x1];
  b = hg->point[(y1 * hg->x_size) + x2];
  c = hg->point[(y2 * hg->x_size) + x1];
  d = hg->point[(y2 * hg->x_size) + x2];

  return (x + y < 1) ? (
    a + ((b-a) * (0+x)) + ((c-a) * (0+y))
  ) : (
    d + ((c-d) * (1-x)) + ((b-d) * (1-y))
  );
}

/*
[PUBLIC] Export a HeightGrid structure as an OBJ mesh file.
*/

hg_bool hgExportOBJ(const HeightGrid *hg, const char *file_name) {
  FILE *out;
  hg_int x, y;

  if (!hg || !file_name)
    return HG_FALSE;

  out = fopen(file_name, "wb");
  if (!out)
    return HG_FALSE;

  for (y = 0; y < hg->y_size; ++y) {
    for (x = 0; x < hg->x_size; ++x) {
      fprintf(out, "v %lu %lu %f\n", x, y, hgGetValue(hg, x, y));
      if (x > 0 && y > 0) {
        fprintf(out, "f %lu %lu %lu\n",
          ((y-1) * hg->x_size) + (x-1) + 1,
          ((y-1) * hg->x_size) + (x-0) + 1,
          ((y-0) * hg->x_size) + (x-1) + 1
        );
        fprintf(out, "f %lu %lu %lu\n",
          ((y-0) * hg->x_size) + (x-0) + 1,
          ((y-0) * hg->x_size) + (x-1) + 1,
          ((y-1) * hg->x_size) + (x-0) + 1
        );
      }
    }
  }

  fclose(out);
  return HG_TRUE;
}
